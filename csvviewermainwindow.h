#ifndef CSVVIEWERMAINWINDOW_H
#define CSVVIEWERMAINWINDOW_H

#include <QMainWindow>
#include <QFile>
#include <QDebug>
#include <vector>
#include "advqcstmpltt.h"

QT_BEGIN_NAMESPACE
namespace Ui { class CsvViewerMainWindow; }
QT_END_NAMESPACE

class CsvViewerMainWindow : public QMainWindow
{
    Q_OBJECT

public:
    CsvViewerMainWindow(QWidget *parent = nullptr);
    void loadFile(QString path);
    ~CsvViewerMainWindow() override;

private:
    Ui::CsvViewerMainWindow *ui;
    QFileDialog m_dialog;

    bool loadData(QString path);


public slots:
    void hndl_slct_ch();
    void hndl_actionOpenFile();
    void hndl_fileAccept();
    void dropEvent(QDropEvent* event) override;
    void dragEnterEvent(QDragEnterEvent * evt) override;

};
#endif // CSVVIEWERMAINWINDOW_H
