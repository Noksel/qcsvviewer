#include "csvviewermainwindow.h"
#include "ui_csvviewermainwindow.h"

CsvViewerMainWindow::CsvViewerMainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::CsvViewerMainWindow)
{
    ui->setupUi(this);

    this->setAcceptDrops(true);

    // loadData("test.csv");

    connect(ui->tableWidget,&QTableWidget::itemSelectionChanged,this,&CsvViewerMainWindow::hndl_slct_ch);
    connect(ui->actionOpen,&QAction::triggered,this,&CsvViewerMainWindow::hndl_actionOpenFile);
    connect(&m_dialog,&QFileDialog::accepted,this,&CsvViewerMainWindow::hndl_fileAccept);

    m_dialog.setViewMode(QFileDialog::List);
    m_dialog.setFileMode(QFileDialog::AnyFile);
    m_dialog.setNameFilter(tr("CSV (*.csv *.txt)"));

    //  hndl_slct_ch();
    ui->widget->legend->setVisible(true);
}

void CsvViewerMainWindow::loadFile(QString path)
{
    qDebug()<<path;
    loadData(path);
    hndl_slct_ch();
}

CsvViewerMainWindow::~CsvViewerMainWindow()
{
    delete ui;
}

bool CsvViewerMainWindow::loadData(QString path)
{
    QFile file(path);
    if(!file.open(QIODevice::ReadOnly)){
        ui->statusbar->showMessage(tr("File not found"));
        return false;
    }
    QString first_line = file.readLine();
    first_line.chop(2);
    QRegExp rx("[A-Za-z]+");

    QString delimiter=";";

    QFileInfo fileInfo(file.fileName());


    this->setWindowTitle(fileInfo.fileName());



    if ((first_line.contains("[")||first_line.contains(rx))&&!first_line.contains("NaN"))
    {
        QStringList header = first_line.split(delimiter,QString::KeepEmptyParts);
        if (first_line.contains(",")){
            delimiter=",";
            header = first_line.split(delimiter,QString::KeepEmptyParts);

        }
        qDebug()<<delimiter;

        ui->tableWidget->setColumnCount(header.size());
        // qDebug()<<header;
        ui->tableWidget->setHorizontalHeaderLabels(header);
    }
    bool comma_dec_sep=false;

    QString line = file.readLine();
    if (delimiter==";"&&line.contains(","))
        comma_dec_sep=true;

    int rows=0;
    while (!file.atEnd()) {
        line.chop(2);
        rows++;
        ui->tableWidget->setRowCount(rows);
        QStringList data = line.split(delimiter,QString::KeepEmptyParts);

        for (int column=0;column<data.size();++column){
            QString val = data.at(column);
            if(comma_dec_sep){
                val.replace(",",".");
            }

            QTableWidgetItem *newItem = new QTableWidgetItem(val);
            ui->tableWidget->setItem(rows-1, column, newItem);
        }
        line = file.readLine();
    }

    return true;

}

void CsvViewerMainWindow::hndl_slct_ch()
{

    ui->widget->clearGraphs();

    QList<QTableWidgetItem *>lst = ui->tableWidget->selectedItems();

    if (lst.size()==0){
        for (int row = 0 ; row < ui->tableWidget->rowCount() ; ++row) {
            for (int col = 0 ; col < ui->tableWidget->columnCount() ; ++col) {
                lst << ui->tableWidget->item(row, col);
            }
        }
    }


    //    qDebug()<<"========";

    int clmns =0;
    std::vector<int> clmn;
    std::vector<int> rows;
    QVector<double> x;

    QVector<QVector<double>> ys;

    if (lst.size()==0)
        clmns = ui->tableWidget->rowCount();
    else
    {
        foreach (QTableWidgetItem * it, lst) {
            //  qDebug()<<it->row()+1<<it->column()+1;
            //    it->data()

            if (std::find(clmn.begin(),clmn.end(),it->column())==clmn.end())
            {
                clmn.push_back(it->column());
                ys.push_back(QVector<double>());
                // clmn.push_back(it->column());
                if (clmn.size()!=2){
                    QCPGraph * grp= ui->widget->addGraph();
                }
            }

            if (std::find(rows.begin(),rows.end(),it->row())==rows.end())
            {
                rows.push_back(it->row());
                x.push_back(it->text().toDouble());
            }

            int id= std::distance(clmn.begin(),std::find(clmn.begin(),clmn.end(),it->column()));
            ys[id].push_back(it->text().toDouble());
        }
        clmns = clmn.size();
    }
    if(clmns>1)
    {
        ui->widget->xAxis->setLabel(ui->tableWidget->horizontalHeaderItem(clmn.at(0))->text());

        ys.remove(0);
        clmn.erase(clmn.begin());
        --clmns;
    }
    else
    {
        std::iota(x.begin(),x.end(),0);
        ui->widget->xAxis->setLabel(tr("Counts"));
    }

    //   qDebug()<<"Graphs count:"<<clmns;
    //   qDebug()<<x;

    for (int i =0;i<ys.size();++i)
    {
        ui->widget->graph(i)->setData(x,ys[i]);
        ui->widget->graph(i)->setName(ui->tableWidget->horizontalHeaderItem(clmn[i])->text());
    }
    ui->widget->rescaleAxes(true);
}

void CsvViewerMainWindow::hndl_actionOpenFile()
{
    m_dialog.exec();
    // QStringList fileNames = m_dialog.selectedFiles();
    //  qDebug()<<fileNames;
}

void CsvViewerMainWindow::hndl_fileAccept()
{
    if (m_dialog.selectedFiles().size()>0){
        loadData(m_dialog.selectedFiles()[0]);
        hndl_slct_ch();
    }


}

void CsvViewerMainWindow::dropEvent(QDropEvent *event)
{
    const QMimeData* mimeData = event->mimeData();
    if (mimeData->hasUrls())
    {
        QStringList pathList;
        QList<QUrl> urlList = mimeData->urls();

        // extract the local paths of the files
        //      for (int i = 0; i < urlList.size() && i < 32; ++i)
        //      {
        //        pathList.append(urlList.at(i).toLocalFile());
        //      }

        qDebug()<<urlList.at(0).toLocalFile();
        loadData(urlList.at(0).toLocalFile());
        hndl_slct_ch();

        // call a function to open the files
        // openFiles(pathList);
    }
}

void CsvViewerMainWindow::dragEnterEvent(QDragEnterEvent * evt) {
    if(evt->mimeData()->hasUrls()&&evt->mimeData()->urls().size()<2){
        QList<QUrl> urlList = evt->mimeData()->urls();
        if (urlList[0].toLocalFile().endsWith(".csv"))
            evt->accept();
    }
}

